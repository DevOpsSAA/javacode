# 用于EE-SET团队试验流水线配置，测试工具的代码库，以下是部署的过程:
#安装docker
#: git clone 代码库 /baidu/ee-set/javacode
#: 安装数据库:
	## 启动容器，执行: docker run -d -p 3308:3306 --name mysql -e MYSQL_ROOT_PASSWORD=12345 -v /${PWD}/data/:/databackup mysql:5.7
	## 进入容器，执行: docker exec -it mysql bash
	## 导入数据, 执行: mysql -uroot -p12345 < /databackup/all.sql
	## exit退出容器
#: 运行webservice容器:
	##. 执行docker inspect mysql，查看之前运行的mysql容器的network，记住字符串
	##. 启动web service容器,执行: docker run -d -p 8080:8080 --network bridge --link mysql --name web registry.baidu.com/jinrui01/javaweb:1.0
	##. 执行docker ps -a， 观察mysql和web是否都启动起来了
#: 运行服务:
	##. 浏览器输入: 0.0.0.0:8080/user/all,看是否正确返回结果
#:一些默认设置:
##.所有命令的执行请在代码库根目录下执行
##.mysql root密码: 12345，在step 2-1设置，可以修改，修改后在2-3也要做相应的设置
##.测试数据的备份地址: ./data/all.sql
##.mysql 镜像的外部访问端口号: 3308，在step2-1里设置；
##.webservice的配置文件在./config-back/application.properties，已经打包入镜像，如果有其它修改，请修改后重新打包镜像