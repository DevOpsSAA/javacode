/*Create table users*/
CREATE TABLE IF NOT EXISTS`users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

/*Create table type*/
CREATE TABLE IF NOT EXISTS `abilitytype` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`name` varchar(50) NOT NULL,
`status` int(11) NOT NULL,
PRIMARY KEY (`id`)
);

/*Create table abilities*/
CREATE TABLE IF NOT EXISTS `abilities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `current` int(11) DEFAULT NULL,
  `next` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

/*Create table contributetype*/
CREATE TABLE IF NOT EXISTS `contributetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

/*Create table contribute*/
CREATE TABLE IF NOT EXISTS `contribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `ability` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


