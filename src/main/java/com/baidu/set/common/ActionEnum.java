package com.baidu.set.common;

public enum ActionEnum {
    INSERT,
    UPDATE,
    SELECT,
    DELETE
}
