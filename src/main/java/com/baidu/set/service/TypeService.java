package com.baidu.set.service;

import com.baidu.set.entity.AbilityEntity;
import com.baidu.set.entity.TypeEntity;
import com.baidu.set.repository.IAbilityRepository;
import com.baidu.set.repository.ITypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TypeService {

    private static final int archiveStatus = 0;
    private static final int activeStatus = 1;
    @Autowired
    private ITypeRepository typeRepository;
    @Autowired
    private IAbilityRepository abilityRepository;

    public TypeEntity findTypeById(long id) {
        TypeEntity type = null;
        try {
            type = typeRepository.findTypeById(id);
        } catch (Exception e) {
            return null;
        }
        return type;
    }

    public TypeEntity findTypeByName(String name) {
        TypeEntity type = null;
        try {
            type = typeRepository.findTypeByName(name);
        } catch (Exception e) {
            return null;
        }
        return type;
    }

    public List<TypeEntity> findAllTypes() {
        return typeRepository.findAll();
    }

    public List<TypeEntity> findTypesByStatus(int status) {
        return typeRepository.findTypesByStatus(status);
    }

    public TypeEntity save(TypeEntity type) {
        return typeRepository.saveAndFlush(type);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public TypeEntity archive(long id) {
        TypeEntity currentType = this.typeRepository.findTypeById(id);
        if (null == currentType) {
            return null;
        } else {
            if (archiveStatus == currentType.getStatus()) {
                return null;
            } else {
                List<AbilityEntity> abilityInType = this.abilityRepository.findAbilitiesByType(id);
                for (AbilityEntity ability : abilityInType) {
                    this.abilityRepository.updateAbilityStatus(ability.getId(), archiveStatus);
                }
                currentType.setStatus(archiveStatus);
                return this.typeRepository.save(currentType);
            }
        }
    }

}
