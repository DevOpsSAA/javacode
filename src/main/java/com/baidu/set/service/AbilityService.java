package com.baidu.set.service;

import com.baidu.set.entity.AbilityEntity;
import com.baidu.set.entity.TypeEntity;
import com.baidu.set.repository.IAbilityRepository;
import com.baidu.set.repository.ITypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AbilityService {
    @Autowired
    private IAbilityRepository abilityRepository;
    @Autowired
    private ITypeRepository typeRepository;

    public List<AbilityEntity> findAbilitiesByType(int type) {
        return abilityRepository.findAbilitiesByType(type);
    }

    public AbilityEntity findAbilityById(long id) {
        AbilityEntity ability = null;
        try {
            ability = abilityRepository.findAbilityById(id);
        } catch (Exception e) {
            return null;

        }
        return ability;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public String updateAbilityType(long id, long type) {
        AbilityEntity ability = this.findAbilityById(id);
        TypeEntity typeEntiy = typeRepository.findTypeById(type);
        String reponse = "";
        if (null != ability) {
            if (null != typeEntiy) {
                this.abilityRepository.updateAbilityType(id, type);
                return ReturnStatus.getIndexByName("OK").getName();
            } else {
                return ReturnStatus.getIndexByName("NoResult").getName();
            }
        } else {
            return ReturnStatus.getIndexByName("Error").getName();
        }
    }

    public String updateAbilityStatus(long id, int status) {
        AbilityEntity ability = this.findAbilityById(id);
        if (null != ability) {
            updateAbilityStatus(id, status);
            return ReturnStatus.getIndexByName("OK").getName();
        } else {
            return ReturnStatus.getIndexByName("Error").getName();
        }
    }

    public List<AbilityEntity> findAbilitiesByStatus(int status) {
        return this.abilityRepository.findAbilitiesByStatus(status);
    }

    public AbilityEntity insertNewAbility(String name, String description, long type) {
        AbilityEntity ability = new AbilityEntity();
        ability.setName(name);
        ability.setDescription(description);
        ability.setType(type);
        ability.setCurrent(0);
        ability.setTotal(8);
        ability.setNext(0);
        try {
            return abilityRepository.saveAndFlush(ability);
        } catch (Exception e) {
            return null;
        }
    }

    public int calculateTotalScore() {
        return abilityRepository.getTotalScore();
    }

    public int calculateCurrentScore() {
        return abilityRepository.getCurrentScore();
    }

}
