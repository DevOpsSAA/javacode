package com.baidu.set.service;

public enum ReturnStatus {

    OK("OK", 200),
    NoResult("No result", 300),
    Error("Error", 400);

    private String name;
    private int number;

    ReturnStatus(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public static ReturnStatus getIndexByName(String name) {
        if (null == name) {
            return null;
        }
        for (ReturnStatus status : ReturnStatus.values()) {
            if (status.getName() == name) {
                return status;
            }
        }
        return null;

    }

    public String getName() {
        return this.name;
    }

    public int getNumber() {
        return this.number;
    }

}
