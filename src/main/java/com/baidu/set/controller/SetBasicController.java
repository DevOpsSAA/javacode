package com.baidu.set.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class SetBasicController {

    protected ReturnObject composeByOne(AbstractEntity entity, ActionEnum action, String responseWords) {
        List<AbstractEntity> entityList = new ArrayList<>();
        if (null != entity) {
            entityList.add(entity);
        }
        ReturnObject obj = new ReturnObject();
        obj.setEntities(entityList);
        obj.composeObject(action);
        obj.setResponseWords(responseWords);
        return obj;
    }

    protected ReturnObject composeByList(List<AbstractEntity> entities, ActionEnum action, String responseWords) {
        ReturnObject obj = new ReturnObject();
        obj.setEntities(entities);
        obj.composeObject(action);
        obj.setResponseWords(responseWords);
        return obj;
    }

    protected ReturnObject composeByNumber(int result, ActionEnum action, String responseWords) {
        ReturnObject obj = new ReturnObject();
        return obj;
    }
}
