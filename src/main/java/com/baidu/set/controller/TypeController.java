package com.baidu.set.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.entity.AbstractEntity;
import com.baidu.set.entity.TypeEntity;
import com.baidu.set.service.TypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/type")
public class TypeController extends SetBasicController {

    private static final int defaultStatus = 1;
    private static final int inactivateStatus = 0;
    @Autowired
    private TypeService typeService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject findAllTypes() {
        List<TypeEntity> typeList = typeService.findAllTypes();
        List<AbstractEntity> entities = new ArrayList<>();
        for (TypeEntity type : typeList) {
            entities.add(type);
        }
        return this.composeByList(entities, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/active", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject findAllActiveTypes() {
        List<TypeEntity> typeList = typeService.findTypesByStatus(defaultStatus);
        List<AbstractEntity> entities = new ArrayList<>();
        for (TypeEntity type : typeList) {
            entities.add(type);
        }
        return this.composeByList(entities, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/inactive", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject findAllInactiveTypes() {
        List<TypeEntity> typeList = typeService.findTypesByStatus(inactivateStatus);
        List<AbstractEntity> entities = new ArrayList<>();
        for (TypeEntity type : typeList) {
            entities.add(type);
        }
        return this.composeByList(entities, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/byId", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject findTypeById(@RequestParam(value = "id") long id) {
        TypeEntity type = this.typeService.findTypeById(id);
        return this.composeByOne(type, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject insertType(@RequestParam(value = "name") String name) {
        TypeEntity existingType = this.typeService.findTypeByName(name);
        String responseWords = "";
        if (null == existingType) {
            existingType = new TypeEntity();
            existingType.setName(name);
            existingType.setStatus(defaultStatus);
            TypeEntity newType = typeService.save(existingType);
            return this.composeByOne(newType, ActionEnum.INSERT, "");
        } else {
            if (1 == existingType.getStatus()) {
                responseWords = String.format("Found duplicate name at active type: %s.", name);
                return this.composeByOne(null, ActionEnum.INSERT, responseWords);
            } else {
                responseWords = String.format("Found duplicate name at archive type: %s.", name);
                return this.composeByOne(null, ActionEnum.INSERT, responseWords);
            }
        }
    }

    @RequestMapping(value = "/restore", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject restoreType(@RequestParam(value = "id") long id) {
        TypeEntity existingType = this.typeService.findTypeById(id);
        ReturnObject obj;
        String responseWords = "";
        if (null == existingType) {
            responseWords = String.format("Found no result at id: %s", String.valueOf(id));
            obj = this.composeByOne(null, ActionEnum.UPDATE, responseWords);
        } else {
            if (1 == existingType.getStatus()) {
                responseWords = String.format("Type: %s is already activated.", existingType.getName());
                obj = this.composeByOne(null, ActionEnum.UPDATE, responseWords);
            } else {
                existingType.setStatus(defaultStatus);
                TypeEntity activeType = this.typeService.save(existingType);
                obj = this.composeByOne(activeType, ActionEnum.UPDATE, responseWords);
            }
        }
        return obj;
    }

    @RequestMapping(value = "/del", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject archiveType(@RequestParam(value = "id") long id) {
        TypeEntity existingType = this.typeService.archive(id);
        ReturnObject obj;
        String responseWords = "";
        if (null == existingType) {
            responseWords = String.format("Found no available result at id: %s", String.valueOf(id));
            obj = this.composeByOne(null, ActionEnum.DELETE, responseWords);
        } else {
            obj = this.composeByOne(existingType, ActionEnum.DELETE, "");
        }
        return obj;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject updateType(@RequestParam(value = "id") long id,
                                   @RequestParam(value = "name") String name,
                                   @RequestParam(value = "status") int status) {
        TypeEntity currentType = typeService.findTypeById(id);
        String responseWords = "";
        ReturnObject obj;
        if (null == currentType) {
            responseWords = String.format("Found no result at id: %s.", String.valueOf(id));
            obj = this.composeByOne(null, ActionEnum.UPDATE, responseWords);
        } else {
            TypeEntity duplicateType = typeService.findTypeByName(name);
            if (null == duplicateType) {
                currentType = new TypeEntity();
                currentType.setName(name);
                currentType.setStatus(status);
                currentType = this.typeService.save(currentType);
                obj = this.composeByOne(currentType, ActionEnum.UPDATE, "");
            } else {
                responseWords = String.format("Found duplicate type name at name: %s.", name);
                obj = this.composeByOne(null, ActionEnum.UPDATE, responseWords);
            }
        }
        return obj;
    }
}
