package com.baidu.set.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.entity.AbstractEntity;
import com.baidu.set.entity.UserEntity;
import com.baidu.set.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController extends SetBasicController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject allUser() {
        List<UserEntity> allUser = userService.findAll();
        List<AbstractEntity> entities = new ArrayList<>();
        for (UserEntity user : allUser) {
            entities.add(user);
        }
        return this.composeByList(entities, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/show", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject show(@RequestParam(value = "name") String name) {
        UserEntity user = userService.findUserByName(name);
        return this.composeByOne(user, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/byid", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject showById(@RequestParam(value = "id") long id) {
        UserEntity user = userService.findUserById(id);
        return this.composeByOne(user, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject addorupdate(@RequestParam(value = "id") long id,
                                    @RequestParam(value = "name") String name,
                                    @RequestParam(value = "email") String email) {
        ReturnObject obj;
        UserEntity user = userService.findUserById(id);
        UserEntity existingUser = userService.findUserByEmail(email);
        String responseWords = "";
        if (null == user) {
            if (null == existingUser) {
                user = new UserEntity();
                user.setName(name);
                user.setEmail(email);
                user = userService.save(user);
                obj = this.composeByOne(user, ActionEnum.INSERT, responseWords);
            } else {
                responseWords = String.format("Duplicate email: %s, please use another one.", email);
                obj = this.composeByOne(null, ActionEnum.INSERT, responseWords);
            }
            return obj;
        } else {
            UserEntity newUser = null;
            if (null == existingUser) {
                newUser = new UserEntity();
                newUser.setName(name);
                newUser.setEmail(email);
                newUser = userService.save(newUser);
                obj = this.composeByOne(newUser, ActionEnum.UPDATE, responseWords);
            } else {
                responseWords = String.format("Duplicate email: %s, please use another one.", email);
                obj = this.composeByOne(null, ActionEnum.UPDATE, responseWords);
            }
            return obj;
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject delete(@RequestParam(value = "id") long id) {
        UserEntity user = userService.findUserById(id);
        String responseWords;
        if (null == user) {
            responseWords = String.format("UserId: %s doesn't exist", String.valueOf(id));

        } else {
            String userName = user.getName();
            userService.delete(user);
            responseWords = String.format("Delete user: %s successful", userName);
        }
        return this.composeByOne(user, ActionEnum.DELETE, responseWords);
    }

}
