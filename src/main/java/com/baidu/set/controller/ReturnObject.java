package com.baidu.set.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.common.CallStatus;
import com.baidu.set.entity.AbstractEntity;

import java.util.List;

public class ReturnObject {

    private List<AbstractEntity> entities;
    private CallStatus status;
    private ActionEnum action;
    private int size;
    private String responseWords;

    public ReturnObject() {

    }

    public ReturnObject(List<AbstractEntity> entityList) {
        this.entities = entityList;
    }

    public ActionEnum getAction() {
        return this.action;
    }

    public void setAction(ActionEnum action) {
        this.action = action;
    }

    public CallStatus getStatus() {
        return this.status;
    }

    public void setStatus(CallStatus statusCode) {
        this.status = statusCode;
    }

    public List<AbstractEntity> getEntities() {
        return this.entities;
    }

    public void setEntities(List<AbstractEntity> entity) {
        this.entities = entity;
    }

    public int getSize() {
        this.size = entities.size();
        return this.size;
    }

    public String getResponseWords() {
        return this.responseWords;
    }

    public void setResponseWords(String words) {
        this.responseWords = words;
    }

    public void composeObject(ActionEnum action) {
        if (this.entities.size() > 0) {
            this.setStatus(CallStatus.NORMAL);
        } else {
            this.setStatus(CallStatus.NORESULT);
        }
        this.setAction(action);
    }

}
