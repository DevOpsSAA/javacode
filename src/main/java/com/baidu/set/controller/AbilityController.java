package com.baidu.set.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.entity.AbilityEntity;
import com.baidu.set.entity.AbstractEntity;
import com.baidu.set.service.AbilityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/ability")
public class AbilityController extends SetBasicController {

    private static final int archiveStatus = 0;
    private static final int activeStatus = 1;
    @Autowired
    private AbilityService abilityService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject insertNewAbility(@RequestParam(value = "name") String name,
                                         @RequestParam(value = "description") String description,
                                         @RequestParam(value = "type") int type) {
        AbilityEntity ae = abilityService.insertNewAbility(name, description, type);
        String responseWords;
        if (null == ae) {
            responseWords = String.format("Insert status: %s", "Wrong");
        } else {
            responseWords = String.format("Insert status: %s", "OK");
        }
        return this.composeByOne(ae, ActionEnum.INSERT, responseWords);
    }

    @RequestMapping(value = "/showarchive", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject showArchivedAbilities() {
        List<AbilityEntity> archivedAbilities = this.abilityService.findAbilitiesByStatus(archiveStatus);
        List<AbstractEntity> entityList = new ArrayList<>();
        for (AbilityEntity ability : archivedAbilities) {
            entityList.add(ability);
        }
        return this.composeByList(entityList, ActionEnum.SELECT, "");
    }

    @RequestMapping(value = "changetype", method = RequestMethod.GET)
    @ResponseBody
    public ReturnObject updateType(@RequestParam(value = "id") long id,
                                   @RequestParam(value = "type") long type) {
        String responseWords = abilityService.updateAbilityType(id, type);
        AbilityEntity ae = null;
        switch (responseWords) {
            case "OK":
                ae = abilityService.findAbilityById(id);
                break;
            case "Error":
                break;
            case "NoResult":
                break;
            default:
                break;
        }
        return this.composeByOne(ae, ActionEnum.UPDATE, responseWords);

    }
}
