package com.baidu.set.repository;

import com.baidu.set.entity.ContributeEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IContributeRepository extends JpaRepository<ContributeEntity, Long> {

    @Query("select t from ContributeEntity t where t.user = :user")
    List<ContributeEntity> findContributesByUserId(@Param(value = "user") long user);

    @Query("select t from ContributeEntity t where t.ability = :ability")
    List<ContributeEntity> findContributesByAbilityId(@Param(value = "ability") long ability);

    @Query("select t from ContributeEntity t where t.id = :id")
    ContributeEntity findContributesById(@Param(value = "id") long id);
}
