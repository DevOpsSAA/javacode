package com.baidu.set.repository;

import com.baidu.set.entity.TypeEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITypeRepository extends JpaRepository<TypeEntity, Long> {

    @Query("select t from TypeEntity t where t.id = :id")
    TypeEntity findTypeById(@Param(value = "id") long id);

    @Query("select t from TypeEntity t where t.name = :name")
    TypeEntity findTypeByName(@Param(value = "name") String name);

    @Query("select t from TypeEntity t where t.status = :status")
    List<TypeEntity> findTypesByStatus(@Param(value = "status") int status);

}
