package com.baidu.set.repository;

import com.baidu.set.entity.AbilityEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAbilityRepository extends JpaRepository<AbilityEntity, Long> {

    @Query("select t from AbilityEntity t where t.id = :id")
    AbilityEntity findAbilityById(@Param("id") long id);

    @Query("select t from AbilityEntity t where t.type = :type")
    List<AbilityEntity> findAbilitiesByType(@Param("type") long type);

    @Query("select t from AbilityEntity t where t.status = :status")
    List<AbilityEntity> findAbilitiesByStatus(@Param("status") int status);

    @Modifying
    @Query("update AbilityEntity ae set ae.type= :type where ae.id = :id")
    void updateAbilityType(@Param("id") long id, @Param("type") long type);

    @Modifying
    @Query("update AbilityEntity ae set ae.status= :status where ae.id = :id")
    void updateAbilityStatus(@Param("id") long id, @Param("status") int status);

    @Modifying
    @Query("update AbilityEntity ae set ae.current = :current, ae.next= :next where ae.id= :id")
    void updateCurrentScore(@Param("id") long id, @Param("current") int currentScore, @Param("next") int next);

    @Query("select sum(t.total) from AbilityEntity t")
    int getTotalScore();

    @Query("select sum(t.current) from AbilityEntity t")
    int getCurrentScore();

    @Query("select sum(t.total) from AbilityEntity t where t.type= :type")
    int getOneTypeTotalScore(@Param("type") long type);

    @Query("select sum(t.current) from AbilityEntity t where t.type = :type")
    int getOneTypeCurrentScore(@Param("type") long type);

}