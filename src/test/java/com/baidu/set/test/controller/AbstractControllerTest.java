package com.baidu.set.test.controller;

import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractControllerTest {

    @Value("${spring.datasource.username}")
    protected String dbUser;
    @Value("${spring.datasource.password}")
    protected String dbPassword;
    @Value("${spring.datasource.url}")
    protected String dbUrl;
    @Value("${spring.datasource.driver-class-name}")
    protected String dbDriver;

    protected String abilityMapping = "/ability/";
    protected String userMapping = "/user/";
    protected String typeMapping = "/type/";

    protected String insertUser = "INSERT INTO `users`(id,name,email) VALUES (1,'Tom','tom01@baidu.com'),"
            + "(2,'jinrui02','jinrui01@baidu.com'),"
            + "(3,'chenxiaosheng','chenxiaosheng@baidu.com'),"
            + "(4,'leitao','leitao01@baidu.com'),"
            + "(5,'gaoyanan01','gaoyanan01@baidu.com'),"
            + "(6,'wangping','wangping06@baidu.com'),"
            + "(7,'zhangtao','zhangtao09@baidu.com'),"
            + "(8,'wangping','wangping06@baidu.com'),"
            + "(9,'changhong','changhong@baidu.com'),"
            + "(10,'lichun','lichun03@baidu.com');";

    protected String insertAbility = "INSERT INTO `abilities`(id,name,description,type,total,current,next,status) "
            + "VALUES (1,'story mapping','storymapping based',4,8,0,0,1),"
            + "(2,'test name','description',4,8,0,0,1),"
            + "(3,'Lean startup','Lean startup, and business canvas',4,8,0,0,0);";

    protected String insertType = "INSERT INTO `abilitytype`(id,name,status) "
            + "VALUES (1,'data analytics',1),"
            + "(2,'requirement management',1),"
            + "(3,'develop model',1),"
            + "(4,'standard',0);";
}
