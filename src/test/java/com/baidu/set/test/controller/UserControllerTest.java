package com.baidu.set.test.controller;

import com.baidu.set.common.ActionEnum;
import com.baidu.set.common.CallStatus;
import com.baidu.set.controller.UserController;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest extends AbstractControllerTest {
    private MockMvc mockMvc;
    private String updateString = "update?id={id}&name={name}&email={email}";
    private String deleteString = "delete?id={id}";
    @Autowired
    private WebApplicationContext webContext;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webContext).build();
        Class.forName(this.dbDriver);
        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute(this.insertUser);
        } catch (Exception e) {
            // throw exception
        }
    }

    @After
    public void tearDown() throws Exception {
        Class.forName(this.dbDriver);
        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute("Delete from users");
        } catch (Exception e) {
            // throw exception
        }
    }

    @Test
    public void TestConnection() throws Exception {
        Class.forName(this.dbDriver);
        Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("select * from users;");
        int count = 0;
        while (rs.next()) {
            count++;
        }
        Assert.assertEquals(10, count);
    }

    @Test
    public void TestGetAllUsers() throws Exception {
        MvcResult result = mockMvc.perform(get(userMapping + "all"))
                .andExpect(status().isOk())
                .andExpect(content().encoding("UTF-8"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(10))
                .andReturn();
    }

    @Test
    public void TestGetSingleUser() throws Exception {
        String name = "chenxiaosheng";
        MvcResult result = mockMvc.perform((get(userMapping + "show?name={name}", name)))
                .andExpect(status().isOk())
                .andExpect(handler().handlerType(UserController.class))
                .andExpect(content().encoding("UTF-8"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("size").value(1))
                .andExpect(jsonPath("entities").isArray())
                .andReturn();
    }

    @Test
    public void TestGetNoneExistsUser() throws Exception {
        MvcResult result = mockMvc.perform(get(userMapping + "show?name={name}", "john doe"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("size").value(0))
                .andReturn();
    }

    @Test
    public void TestGetUserById() throws Exception {
        MvcResult result = mockMvc.perform(get(userMapping + "byid?id={id}", 3))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("size").value(1))
                .andReturn();
    }

    @Test
    public void TestGetUserByNoneExistId() throws Exception {
        MvcResult result = mockMvc.perform(get(userMapping + "byid?id={id}", 15))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("size").value(0))
                .andReturn();
    }

    @Test
    public void TestGetUserByInvalidId() throws Exception {
        MvcResult result = mockMvc.perform(get(userMapping + "byid?id={id}", "invalid"))
                .andExpect(status().is4xxClientError()).andReturn();
    }

    @Test
    public void TestUpdateCurrentUser() throws Exception {
        long id = 2;
        String name = "jinrui02";
        String email = "jinrui05@baidu.com";
        MvcResult result = mockMvc.perform(get(userMapping + updateString, id, name, email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("action").value(ActionEnum.UPDATE.toString()))
                .andExpect(jsonPath("size").value(1))
                .andReturn();
    }

    @Test
    public void TestInsertNewUser() throws Exception {
        long id = 16;
        String name = "dingxiaojiao";
        String email = "dingxiaojiao@baidu.com";
        MvcResult result = mockMvc.perform(get(userMapping + updateString, id, name, email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("action").value(ActionEnum.INSERT.toString()))
                .andExpect(jsonPath("size").value(1))
                .andReturn();
    }

    @Test
    public void TestInsertNewUserWithExistsEmail() throws Exception {
        long id = 16;
        String name = "Alison";
        String email = "chenxiaosheng@baidu.com";
        String expectWords = String.format("Duplicate email: %s, please use another one.", email);
        MvcResult result = mockMvc.perform(get(userMapping + updateString, id, name, email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("responseWords").value(expectWords))
                .andExpect(jsonPath("size").value(0))
                .andReturn();
    }

    @Test
    public void TestUpdateExistsEmailUser() throws Exception {
        long id = 2;
        String name = "jinrui03";
        String email = "chenxiaosheng@baidu.com";
        String expectWords = String.format("Duplicate email: %s, please use another one.", email);
        MvcResult result = mockMvc.perform(get(userMapping + updateString, id, name, email))
                .andExpect(status().isOk())
                .andExpect((jsonPath("status").value(CallStatus.NORESULT.toString())))
                .andExpect(jsonPath("responseWords").value(expectWords))
                .andReturn();
    }

    @Test
    public void TestDeleteExistingUser() throws Exception {
        long id = 3;
        MvcResult result = mockMvc.perform(get(userMapping + deleteString, id))
                .andExpect(status().isOk())
                .andReturn();
        MvcResult searchResult = mockMvc.perform(get(userMapping + "byid?id={id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andReturn();
    }

    @Test
    public void TestDeleteNoneExistUser() throws Exception {
        long id = 22;
        String responseWords = String.format("UserId: %s doesn't exist", String.valueOf(id));
        MvcResult result = mockMvc.perform(get(userMapping + deleteString, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("responseWords").value(responseWords))
                .andReturn();
        MvcResult userCountResult = mockMvc.perform(get(userMapping + "all"))
                .andExpect(status().isOk())
                .andExpect(content().encoding("UTF-8"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(10))
                .andReturn();

    }
}