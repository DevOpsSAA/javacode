package com.baidu.set.test.controller;

import com.baidu.set.common.CallStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TypeControllerTest extends AbstractControllerTest {
    @Autowired
    private WebApplicationContext webContext;
    private MockMvc mockMvc;
    private String addString = "add?name={name}";
    private String updateString = "update?id={id}&name={name}&status={status}";
    private String deleteString = "del?id={id}";
    private String selectString = "byId?id={id}";

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webContext).build();
        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute(this.insertType);
            statement.execute(this.insertAbility);
        } catch (Exception e) {
            // throw exception
        }
    }

    @After
    public void tearDown() throws Exception {
        Class.forName(this.dbDriver);
        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute("Delete from abilitytype");
            statement.execute("Delete from ability");
        } catch (Exception e) {
            // throw exception
        }
    }

    @Test
    public void contextLoads() {
        // This is a default method
    }

    @Test
    public void TestFindTypeById() throws Exception {
        long id = 1;
        MvcResult result = mockMvc.perform(get(this.typeMapping + selectString, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("size").value(1))
                .andReturn();
    }

    @Test
    public void TestFindAllTypes() throws Exception {
        MvcResult result = mockMvc.perform(get(this.typeMapping + "all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(4))
                .andReturn();
    }

    @Test
    public void TestInsertAbilityType() throws Exception {
        String duplicateType = "devops";
        MvcResult result = mockMvc.perform(get(this.typeMapping + addString, duplicateType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(1))
                .andReturn();
        MvcResult countResult = mockMvc.perform(get(this.typeMapping + "all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(5))
                .andReturn();
    }

    @Test
    public void TestInsertDuplicateAbilityType() throws Exception {
        String duplicateType = "data analytics";
        MvcResult result = mockMvc.perform(get(this.typeMapping + addString, duplicateType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(0))
                .andReturn();
    }

    @Test
    public void TestUpdateNoneExistsType() throws Exception {
        long id = 7;
        String updateName = "whatever";
        int status = 1;
        MvcResult result = mockMvc.perform(get(this.typeMapping + updateString, id, updateName, status))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(0))
                .andReturn();
    }

    @Test
    public void TestUpdateAbilityType() throws Exception {
        long id = 1;
        String updateName = "data analyze";
        int status = 1;
        MvcResult result = mockMvc.perform(get(this.typeMapping + updateString, id, updateName, status))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(1))
                .andReturn();
    }

    @Test
    public void TestUpdateDuplicateAbilityType() throws Exception {
        long id = 1;
        String updateName = "requirement management";
        int status = 1;
        String responseWords = String.format("Found duplicate type name at name: %s.", updateName);
        MvcResult result = mockMvc.perform(get(this.typeMapping + updateString, id, updateName, status))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORESULT.toString()))
                .andExpect(jsonPath("entities").isArray())
                .andExpect(jsonPath("size").value(0))
                .andExpect(jsonPath("responseWords").value(responseWords))
                .andReturn();
    }

    @Test
    public void TestArchiveType() throws Exception {
        long id = 1;
        MvcResult result = mockMvc.perform(get(this.typeMapping + deleteString, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andReturn();
        MvcResult searchResult = mockMvc.perform(get(this.typeMapping + "active"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value(CallStatus.NORMAL.toString()))
                .andExpect(jsonPath("size").value(2))
                .andReturn();
    }
}
