package com.baidu.set.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AbilityControllerTest extends AbstractControllerTest {
    private MockMvc mockMvc;
    private String prefix = "/ability/";
    @Autowired
    private WebApplicationContext webContext;

    @Test
    public void contextLoads() {
    }

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webContext).build();
        Class.forName(this.dbDriver);
        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute(this.insertAbility);
            statement.execute(this.insertUser);
        } catch (Exception e) {
            // throw exception
        }
    }

    @After
    public void tearDown() throws Exception {
        Class.forName(this.dbDriver);

        try (Connection conn = DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPassword);
             Statement statement = conn.createStatement()) {
            statement.execute("Delete from users");
            statement.execute("Delete from abilities");
            statement.execute("Delete from contributetype");
            statement.execute("Delete from contribute");
        } catch (Exception e) {
            // throw exception
        }
    }

    @Test
    public void TestInsertNewAbility() throws Exception {
        String name = "test name";
        String description = "description";
        long type = 1;
        MvcResult result = mockMvc.perform((get(prefix + "add?name={name}&description={description}&type={type}",
                name, description, type)))
                .andReturn();
    }

    @Test
    public void TestInsertDuplicateAbility() {

    }

    @Test
    public void TestInsertNoneExistsTypeAbility() {

    }

    @Test
    public void TestChangeAbilityTpe() {

    }

    @Test
    public void TestChangeNoneExistsType() {

    }
}
