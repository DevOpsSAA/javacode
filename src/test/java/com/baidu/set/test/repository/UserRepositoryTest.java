package com.baidu.set.test.repository;

import com.baidu.set.entity.UserEntity;
import com.baidu.set.repository.IUserRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private IUserRepository userRepository;

    @Before
    public void SetUp() {
        this.entityManager.persist(new UserEntity("jinrui", "jinrui01@baidu.com"));
        this.entityManager.persist(new UserEntity("wangjiaruo", "wangjiaruo@baidu.com"));
        this.entityManager.persist(new UserEntity("zhaomin", "zhaomin@baidu.com"));
        this.entityManager.persist(new UserEntity("shalong", "shalong01@baidu.com"));
        this.entityManager.persist(new UserEntity("mark", "mark01@baidu.com"));
    }

    @After
    public void CleanUp() {
        this.entityManager.clear();
    }

    @Test
    @Category(com.baidu.set.test.UnitTest.class)
    public void TestFindAllUsers() {
        List<UserEntity> userList = userRepository.findAll();
        Assert.assertEquals(userList.size(), 5);
    }

    @Test
    @Category(com.baidu.set.test.UnitTest.class)
    public void TestFindUserByName() {
        UserEntity newUser = userRepository.findByUserName("mark");
        Assert.assertEquals(newUser.getName(), "mark");
    }

    @Test
    @Category(com.baidu.set.test.UnitTest.class)
    public void TestFindNullUser() {
        UserEntity newUser = userRepository.findByUserName("Stan Lee");
        Assert.assertNull(newUser);
    }

    @Test
    public void TestInsertNewUser() {
        UserEntity user = new UserEntity();
        user.setName("Tommy");
        user.setEmail("tommy01@baidu.com");
        UserEntity newUser = userRepository.save(user);
        UserEntity tommy = userRepository.findByUserName("Tommy");
        Assert.assertEquals("tommy01@baidu.com", tommy.getEmail());
    }

}
